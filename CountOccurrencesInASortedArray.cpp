#include <iostream>
#include <stdio.h>
#include <conio.h>
#include <process.h>

using namespace std;

int Count(int k, int* a, int startIndex, int endIndex)
{
	if (endIndex < startIndex)
		return 0;
	if (a[startIndex]>k)
		return 0;
	if (a[endIndex] < k)
		return 0;
	if (a[startIndex == k] && a[endIndex] == k)
		return endIndex - startIndex+1;

	//Birary search part
	int midIndex = (startIndex + endIndex) / 2;
	if (a[midIndex] == k)
		return 1 + Count(k, a, startIndex, midIndex - 1) + Count(k, a, midIndex + 1, endIndex);
	else if (a[midIndex]>k)
		return Count(k, a, startIndex, midIndex - 1);
	else if (a[midIndex] < k)
		return Count(k, a, midIndex + 1, endIndex);

}

int Count_2(int k, int *a, int len)
{
	int count = 0;
	for (int i = 0; i < len; i++)
	{
		if (a[i] == k)
			count++;
	}
	return count;
}

void main()
{
	int a[] = {1,1,1,1,1,1,1,1,1,1,1,2,3,4,5,6 };
	for (int i = 0; i < size(a); i++)
		cout << a[i] << " ";
	cout << size(a)<<endl;
	cout << Count(1, a, 0, size(a)-1)<<endl;
	cout << Count_2(1, a, size(a)-1) << endl;
	system("pause");

}